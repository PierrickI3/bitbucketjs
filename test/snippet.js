import { expect } from './chai';
import { getClient, config } from './util';

import sinon from 'sinon';
import join from 'url-join';
import nockback from './nockback';
import debug from 'debug';

const log = debug('bitbucketjs:test:snippet')

describe('bitbucket.snippet', () => {
  let bitbucket, snippet;

  beforeEach(() => {
    bitbucket = getClient();
  })

  describe('a basic snippet', () => {
    beforeEach(() => {
      return nockback(`snippet/${config.username}.before.json`, bitbucket.snippet.create({
        owner: config.username,
        title: 'test snippet'
      }).then(s => {
        return snippet = s;
      }));
    });

    it('fetching returns the matching snippet', () => {
      sinon.spy(bitbucket.request, 'get');

      return nockback(`snippet/${config.username}.json`,
        bitbucket.snippet.fetch(snippet.owner.username, snippet.id)
          .then(s => {
            expect(s.owner.username).to.equal(config.username);
            expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'snippets', config.username, s.id));
          })
        );
    });

    it('can be renamed', () => {
      return bitbucket.snippet.update(config.username, snippet.id, { title: 'An updated title' })
        .then(s => {
          return expect(s.title).to.equal('An updated title');
        })
    })

    afterEach(() => {
      return nockback(`snippet/${config.username}.after.json`,
        bitbucket.snippet.delete(config.username, snippet.id));
    });

  });

  describe('a snippet with files', () => {
    let snippet;

    beforeEach(() => {
      return nockback('snippet/withfiles.create.json', bitbucket.snippet.create({
        owner: config.username,
        title: 'Snippet with files',
        files: ['fixtures/test.css']
      }).then(s => {
        return snippet = s;
      }))
    });

    it('creates the snippet with the files attached', () => {
      return nockback('snippet/withfiles.fetch.json', bitbucket.snippet.fetch(snippet.owner.username, snippet.id)
        .then(s => {
          return expect(s.files).to.contain.keys('test.css');
        }));
    });

    it('can be updated with more files', () => {
      return nockback('snippet/withfiles.update.json', bitbucket.snippet.update(snippet.owner.username, snippet.id, {
        files: ['fixtures/test.js']
      }).then(s => {
        return expect(s.files).to.contain.keys('test.css', 'test.js');
      }));
    })

    afterEach(() => {
      return nockback('snippet/withfiles.delete.json',
        bitbucket.snippet.delete(config.username, snippet.id));
    });
  });
})
